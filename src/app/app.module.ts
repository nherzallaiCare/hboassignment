import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import {MaterialModule} from './shared/material.module';


import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import {DialogOverviewDialog} from './home/home.component';


import {HBOService} from './services/hbo.service';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    DialogOverviewDialog
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule,
    HttpClientModule
  ],

  entryComponents: [
    DialogOverviewDialog,
  ],
  providers: [HBOService],
  bootstrap: [AppComponent]
})
export class AppModule { }
