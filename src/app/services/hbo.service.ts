import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment';


@Injectable()
export class HBOService {



    constructor(public router: Router, private httpClient: HttpClient) {

    }
    getData() {
        return this.httpClient.get<any>('./assets/mydata.json');
    }

}
