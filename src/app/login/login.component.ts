import { Component, OnInit, ElementRef } from '@angular/core';

import { ParamMap, Router, ActivatedRoute } from '@angular/router';

import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

import {ValidateText} from './../shared/custom.validator';



export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


    hboFormControl = new FormControl('', [
    Validators.required,

  ]);

  matcher = new MyErrorStateMatcher();
  loginForm: FormGroup;
  login = {
    username: '',
    password: ''
  };

  constructor(private route: ActivatedRoute, private router: Router) {

  }

  ngOnInit() {
    if (this.route.snapshot.url[0]) {
    console.log(this.route.snapshot.url[0].path);

    if (this.route.snapshot.url[0].path === 'home') {
      this.router.navigate(['home']);
    }
    document.body.style.background = this.route.snapshot.url[0].path;
    }
    this.loginForm = new FormGroup({

      username: new FormControl('', [Validators.required, ValidateText]),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[A-Za-z\d$@$!%*?&].{7,}')])

      )});
  }
  onSubmit() {
     localStorage.setItem('username', this.loginForm.value.username);
     this.router.navigate(['home']);
  }
}
