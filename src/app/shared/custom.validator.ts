import { AbstractControl } from '@angular/forms';

export function ValidateText(control: AbstractControl) {

    if (control.value !== 'admin') {
        return { validateText: true };
    }

    return null;
}
