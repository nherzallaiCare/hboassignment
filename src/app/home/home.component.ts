import { Component, OnInit, Inject, ViewChild } from '@angular/core';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';

import { Router } from '@angular/router';

import {HBOService} from './../services/hbo.service';

export interface DialogData {
  text: string;
}


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  displayedColumns: string[] = ['Code', 'Name', 'City', 'Country'];
  dataSource = new  MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator;


  text: string;
  username = '';

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewDialog, {
      width: '250px',
      data: {text: this.text}
    });

    dialogRef.afterClosed().subscribe(result => {
     // console.log('The dialog was closed');
    });
  }
  constructor(public dialog: MatDialog, hboSerice: HBOService, private router: Router) {

    if (localStorage.getItem('username')) {
      this.username = localStorage.getItem('username');
    } else {
      this.router.navigate(['login']);
    }
    hboSerice.getData().subscribe(data => {
      ELEMENT_DATA = data;
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      this.dataSource.paginator = this.paginator;
     }, error => {
       console.log(error);
     });

  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  ExportToExcel() {
    // console.log(ELEMENT_DATA);
    // tslint:disable-next-line:no-unused-expression
    new Angular5Csv(ELEMENT_DATA, 'My Report');

  }

}
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dialog-overview--dialog',
  template: `<div> <div>Are you sure</div>
  <button mat-button  (click)="logoutClick()">Yes</button>
  <button mat-button  (click)="onNoClick()">No</button>
  </div>`,
})
// tslint:disable-next-line:component-class-suffix
export class DialogOverviewDialog {

  constructor(private router: Router,
    public dialogRef: MatDialogRef<DialogOverviewDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  logoutClick(): void {
    localStorage.removeItem('username');
    this.router.navigate(['login']);
    this.dialogRef.close();
  }

}
export interface Element {
  Code: string;
  Name: number;
  City: number;
  Country: string;
}
// tslint:disable-next-line:prefer-const
let ELEMENT_DATA: Element[] = [];


